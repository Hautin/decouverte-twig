<?php

namespace App\Controllers;

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

class Controller {
    
    private $container;
    
    public function __construct ($container){
        $this->container= $container;

    }
    public function render(Response $response, $files){

        $this->container->view->render($response, $files);
        
    }
    public function render_args(Response $response, $files, $args){

        $this->container->view->render($response, $files, $args);
        
    }
    public function affichage(Response $response, $sql) {
        $req = $this->container->pdo->prepare($sql);  
        $req->execute();
        return $req->fetchAll();
    }
    public function sreachmed(Response $response, $sql) {
        $req = $this->container->pdo->prepare($sql);  
        $req->execute();
        $resultat=$req->fetch();
        return $resultat;
    }
    public function maj(Response $response, $sql, $tbl) {
        $req = $this->container->pdo->prepare($sql);  
        $req->execute($tbl);
        $res = "tous ces bien passé";
        return $res;
    }
    public function exfetche(Response $response, $sql, $tbl) {
        $req = $this->container->pdo->prepare($sql);  
        $req->execute($tbl);
        $res = $req->fetch();
        return $res;
    }
    public function selectcount(Response $response, $sql, $tbl) {
        $req = $this->container->pdo->prepare($sql);  
        $req->execute($tbl);
        $resultat=$req->rowCount();
        return $resultat;
    }
}