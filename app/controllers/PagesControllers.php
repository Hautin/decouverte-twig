<?php

namespace App\Controllers;

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
    

class PagesControllers extends Controller {

    /* home */
    public function home(Request $request, Response $response){

        $this->render($response, 'pages/home.twig');
        

    }
    /* médocs /// !! il faudra pas oublié le panier */
    public function medicaments(Request $request, Response $response){
        $sql=('SELECT * FROM medicament');
        $aff=$this->affichage($response, $sql);

        $sql=('SELECT * FROM med_prixht');
        $tarif=$this->affichage($response, $sql);
        $sql=('SELECT * FROM med_stock');
        $nbstock=$this->affichage($response, $sql);
        $this->render_args($response, 'pages/medicaments.twig', ['name' => $aff, 'prixht' => $tarif, 'nbstock' => $nbstock ]);
    }
    // description médicament
    public function description_medicaments(Request $request, Response $response, array $args){
        
        //print_r($args);
        $idsreachmed=$args['id'];
        
        $sql=("SELECT * FROM medicament WHERE id_med= '$idsreachmed'");
        $aff=$this->affichage($response, $sql);

        $sql=("SELECT * FROM med_prixht WHERE id_med= '$idsreachmed'");
        $tarif=$this->affichage($response, $sql);

        $sql=("SELECT * FROM med_stock WHERE id_med= '$idsreachmed'");
        $nbstock=$this->affichage($response, $sql);

        $this->render_args($response, 'pages/description-medicament.twig', ['gene' => $aff, 'prixht' => $tarif, 'nbstock' => $nbstock ]);
    }
    

    /* gestion des médicaments ///// il faudra mettre un session_start */
    public function gestion_medicaments(Request $request, Response $response){
        $sql=('SELECT * FROM medicament');
        $rech=$this->affichage($response, $sql);
        $this->render_args($response, 'pages/gestion-medicaments.twig', ['all' => $rech]);
    }
    

    // rechercher med pour gestion
    public function sreach(Request $request, Response $response, array $args){
        $idsreachmed=$request->getParam('sreachmed');
        $sql=("SELECT * FROM medicament WHERE id_med='".$idsreachmed."'");
        $recherche=$this->sreachmed($response, $sql);

        $sql=("SELECT * FROM med_prixht WHERE id_med='".$idsreachmed."'");
        $prixht=$this->sreachmed($response, $sql);
        $sql=("SELECT * FROM med_stock WHERE id_med='".$idsreachmed."'");
        $nbstock=$this->sreachmed($response, $sql);

        $this->render_args($response, 'pages/sreach.twig', ['detail' => $recherche, 'prixht' => $prixht, 'nbstock' => $nbstock]);
    }

    // MAJ des medicament

    public function majbddmed(Request $request, Response $response){
        
        $sql=('SELECT * FROM medicament');
        $aff=$this->affichage($response, $sql);

        $idsreachmed=$_POST['idmed'];
        $namecom=$_POST['namemed'];
        $meddepot=$_POST['meddplegal'];
        
        $cdfam=$_POST['famcode'];
        $medcompo=$_POST['medcomp'];
        $effet=$_POST['effet'];
        $contreindic=$_POST['contreind'];

        $prixmedht=$_POST['prixht'];
        $nbstockmed=$_POST['qtdispo'];

        $sql=("UPDATE medicament SET MED_DEPOTLEGAL= :medlegal, MED_NOMCOMMERCIAL= :namecom, FAM_CODE= :cdfam, MED_COMPOSITION= :medcompo, MED_EFFETS= :effet, MED_CONTREINDIC= :contreindic WHERE id_med= :idmed");
        $tbl = array(
            'medlegal' => $meddepot,
            'namecom' => $namecom,
            'cdfam' => $cdfam,
            'medcompo' => $medcompo,
            'effet' => $effet,
            'contreindic' => $contreindic,
            'idmed' => $idsreachmed);
        $majdetail=$this->maj($response, $sql, $tbl);
            // maj et insert des prix
        $sqlprix=("SELECT * FROM med_prixht WHERE id_med= :idmed");
        $tblprix=array('idmed' => $idsreachmed);
        $prixcount=$this->selectcount($response, $sqlprix, $tblprix);
        if($prixcount==1){ // si le medicament existe dans la table des prix on le met a jour
            $sqlmajprix=("UPDATE med_prixHT SET med_depotLEGAL= :meddepot, med_nomcommercial= :namecom, prix_medHT= :prixmed  WHERE id_med= :idmed");
            $tblmajprix=array(
                'meddepot' => $meddepot,
                'namecom' => $namecom,
                'prixmed' => $prixmedht,
                'idmed' => $idsreachmed
                );
            $majprixht=$this->maj($response, $sqlmajprix, $tblmajprix);
                
        }else{ // on l'insert
            $sqlinsertprix=("INSERT INTO med_prixHT (id_med, med_depotLEGAL, med_nomcommercial, prix_medHT) VALUE(?, ?, ?, ?)");
            $tblinsertprix=array($idsreachmed, $meddepot, $namecom, $prixmedht);
            $inserprixht=$this->maj($response, $sqlinsertprix, $tblinsertprix);
        }
        // maj ou insert des stock
        $sqlstock=("SELECT * FROM med_stock WHERE id_med= :idmed");
        $tblstock=array('idmed' => $idsreachmed);
        $prixcount=$this->selectcount($response, $sqlstock, $tblstock);
        if($prixcount==1){ // si le medicament existe dans la table des stock on le met a jour
            $sqlmajstock=("UPDATE med_prixHT SET med_depotLEGAL= :meddepot, med_nomcommercial= :namecom, nb_stock= :qtstock  WHERE id_med= :idmed");
            $tblmajstock=array(
                'meddepot' => $meddepot,
                'namecom' => $namecom,
                'qtstock' => $nbstockmed,
                'idmed' => $idsreachmed
                );
                $majprixht=$this->maj($response, $sqlmajstock, $tblmajstock);
                
        }else{ // on l'insert
            $sqlinsertstock=("INSERT INTO med_stock (id_med, med_depotLEGAL, med_nomcommercial, nb_stock) VALUE(?, ?, ?, ?)");
            $tblinsertstock=array($idsreachmed, $meddepot, $namecom, $nbstockmed);
            $inserprixht=$this->maj($response, $sqlinsertstock, $tblinsertstock);
        }
        
        $this->render_args($response, 'pages/gestion-medicaments.twig', ['all' => $aff]);
    }

    /* connection preticien */
    public function connection(Request $request, Response $response){
        $this->render($response, 'pages/connection.twig');
        
    }
    public function panier(Request $request, Response $response){
        $this->render($response, 'pages/panier.twig');
        
    }
    public function postconnection(Request $request, Response $response){
        //var_dump($request->getParams());
        echo $_POST['nom'];
        
    }

}


?>