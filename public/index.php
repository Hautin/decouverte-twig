<?php
require '../vendor/autoload.php';


$app = new \Slim\App([
    'settings' => [
        'displayErrorDetails' => true,
        'addContentLengthHeader' => false
    ]
]);

require '../app/container.php';

// home
$app->get('/', \App\Controllers\PagesControllers::class . ':home');

// gestion des medicaments
$app->get('/gestion-medicaments', \App\Controllers\PagesControllers::class . ':gestion_medicaments')->setName('gestion_medicaments');

$app->post('/gestion-medicaments', \App\Controllers\PagesControllers::class . ':majbddmed');

// sreachmed et maj 
$app->get('/sreach', \App\Controllers\PagesControllers::class . ':sreach');


// medicament
$app->get('/medicaments', \App\Controllers\PagesControllers::class . ':medicaments')->setName('medicaments');

// descriptionmedicament
$app->get('/description-medicament/{id}', \App\Controllers\PagesControllers::class . ':description_medicaments')->setName('description-medicament');

// connection
$app->get('/connection', \App\Controllers\PagesControllers::class . ':connection')->setName('connection');

$app->post('/connection', \App\Controllers\PagesControllers::class . ':postconnection');

// panier
$app->get('/panier', \App\Controllers\PagesControllers::class . ':panier')->setName('panier');

$app->run();

?>